<?php
/* =============================================================================
 * 基本設定
 * ========================================================================== */

namespace app\controllers\admin;

use Yii;
use app\models\SettingsModel;
use app\models\SettingsSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;
use app\filters\AccessRule2;


class SettingsController extends Controller
{
    // ----------------------------------------------------
    /**
     * 動作定義
     */
    
    public function behaviors()
    {
        return [
            
            //基本動作制限
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['post'],
                ],
            ],
            
            
            //アクセス制限
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule2::className(),
                ],
                'rules' => [
                    [
                        'actions' => [ 'index', 'create', 'update' ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    // ----------------------------------------------------
    
    /**
     * インデックス
     * @return mixed
     */
    public function actionIndex()
    {
       //データの有る無しを判断してupdateにもっていく
       $cnt = SettingsModel::find()->count();
       if( $cnt == 0 )
       {
           $model = new SettingsModel;
           $model->loadDefaultValues();
           $model->save();
       }
       
       return $this->redirect(['update','id'=>1]);
    }
    
    // ----------------------------------------------------
    /**
     * 変更
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    // ----------------------------------------------------
    
    /**
     * Deletes an existing SettingsModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SettingsModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SettingsModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SettingsModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
