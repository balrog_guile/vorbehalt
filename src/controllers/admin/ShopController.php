<?php
/* =============================================================================
 * ショップ管理
 * ========================================================================== */
namespace app\controllers\admin;

use Yii;
use app\models\ShopModel;
use app\models\ShopSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;
use app\filters\AccessRule2;

class ShopController extends Controller
{
    
    // ----------------------------------------------------
    /**
     * 動作定義
     */
    
    public function behaviors()
    {
        return [
            
            //基本動作制限
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['post'],
                ],
            ],
            
            
            //アクセス制限
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule2::className(),
                ],
                'rules' => [
                    [
                        'actions' => [ 'index', 'create', 'update' ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    // ----------------------------------------------------
    
    /**
     * インデックス
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    // ----------------------------------------------------
    
    /**
     * Displays a single ShopModel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    // ----------------------------------------------------
    
    /**
     * Creates a new ShopModel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShopModel();
        
        if( $model->load(Yii::$app->request->post()) )
        {
            $model->created_at = date('Y-m-d H:i:s');
            $model->update_at = date('Y-m-d H:i:s');
            if( $model->save() )
            {
                return $this->redirect(['index']);
            }
        }
        else
        {
            $model->loadDefaultValues();
        }
        
        
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    // ----------------------------------------------------
    
    /**
     * アップデート
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()))
        {
            if( $model->save() )
            {
                return $this->redirect( ['index'] );
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    // ----------------------------------------------------
    
    /**
     * 削除
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
    
    // ----------------------------------------------------
    
    /**
     * Finds the ShopModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
