<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%settings}}".
 *
 * @property integer $id
 * @property integer $reservation_types
 * @property integer $interval
 * @property integer $init_buffer
 * @property integer $auth_mail
 */
class SettingsModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reservation_types', 'interval', 'init_buffer', 'auth_mail'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'reservation_types' => Yii::t('app', '予約表示タイプ'),
            'interval' => Yii::t('app', 'タイプで時間区切りまたは時間割を選択した場合のインターバル'),
            'init_buffer' => Yii::t('app', '予約バッファを取る場合の分数'),
            'auth_mail' => Yii::t('app', '予約認証'),
        ];
    }
}
