<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[StaffModel]].
 *
 * @see StaffModel
 */
class StaffQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return StaffModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StaffModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}