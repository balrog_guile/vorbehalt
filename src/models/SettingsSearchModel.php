<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SettingsModel;

/**
 * SettingsSearchModel represents the model behind the search form about `app\models\SettingsModel`.
 */
class SettingsSearchModel extends SettingsModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'reservation_types', 'interval', 'init_buffer', 'auth_mail'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SettingsModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'reservation_types' => $this->reservation_types,
            'interval' => $this->interval,
            'init_buffer' => $this->init_buffer,
            'auth_mail' => $this->auth_mail,
        ]);

        return $dataProvider;
    }
}
