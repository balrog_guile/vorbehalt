<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ShopModel]].
 *
 * @see ShopModel
 */
class ShopQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ShopModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ShopModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}