<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StaffModel;

/**
 * StaffSearchModel represents the model behind the search form about `app\models\StaffModel`.
 */
class StaffSearchModel extends StaffModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'shop_id', 'staff_status', 'delete_flag', 'rank', 'shop_role'], 'integer'],
            [['name', 'display_name', 'kana', 'create_at', 'update_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StaffModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'shop_id' => $this->shop_id,
            'staff_status' => $this->staff_status,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
            'delete_flag' => $this->delete_flag,
            'deleted_at' => $this->deleted_at,
            'rank' => $this->rank,
            'shop_role' => $this->shop_role,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'display_name', $this->display_name])
            ->andFilterWhere(['like', 'kana', $this->kana]);

        return $dataProvider;
    }
}
