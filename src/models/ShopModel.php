<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%shop}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $created_at
 * @property string $update_at
 * @property integer $delete_flag
 * @property string $deleted_at
 * @property integer $rank
 */
class ShopModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'update_at'], 'required'],
            [['created_at', 'update_at', 'deleted_at'], 'safe'],
            [['delete_flag', 'rank'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'ショップ名'),
            'created_at' => Yii::t('app', '作成日'),
            'update_at' => Yii::t('app', '修正日'),
            'delete_flag' => Yii::t('app', '削除フラグ'),
            'deleted_at' => Yii::t('app', '削除日'),
            'rank' => Yii::t('app', '表示順'),
        ];
    }

    /**
     * @inheritdoc
     * @return ShopQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShopQuery(get_called_class());
    }
}
