<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%staff}}".
 *
 * @property integer $id
 * @property integer $shop_id
 * @property string $name
 * @property string $display_name
 * @property string $kana
 * @property integer $staff_status
 * @property string $create_at
 * @property string $update_at
 * @property integer $delete_flag
 * @property string $deleted_at
 * @property integer $rank
 * @property integer $shop_role
 */
class StaffModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%staff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_id', 'name', 'display_name', 'create_at', 'update_at'], 'required'],
            [['shop_id', 'staff_status', 'delete_flag', 'rank', 'shop_role'], 'integer'],
            [['create_at', 'update_at', 'deleted_at'], 'safe'],
            [['name', 'display_name', 'kana'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'shop_id' => Yii::t('app', 'ショップID'),
            'name' => Yii::t('app', '名前'),
            'display_name' => Yii::t('app', '表示名'),
            'kana' => Yii::t('app', 'よみ'),
            'staff_status' => Yii::t('app', 'スタッフステータス'),
            'create_at' => Yii::t('app', '作成日'),
            'update_at' => Yii::t('app', '更新日'),
            'delete_flag' => Yii::t('app', '論理削除フラグ'),
            'deleted_at' => Yii::t('app', '削除日'),
            'rank' => Yii::t('app', '表示順'),
            'shop_role' => Yii::t('app', 'ショップ内役割
0:通常スタッフ
1:権限スタッフ'),
        ];
    }

    /**
     * @inheritdoc
     * @return StaffQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StaffQuery(get_called_class());
    }
}
