<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ShopModel;

/**
 * ShopSearchModel represents the model behind the search form about `app\models\ShopModel`.
 */
class ShopSearchModel extends ShopModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'delete_flag', 'rank'], 'integer'],
            [['name', 'created_at', 'update_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'update_at' => $this->update_at,
            'delete_flag' => $this->delete_flag,
            'deleted_at' => $this->deleted_at,
            'rank' => $this->rank,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
