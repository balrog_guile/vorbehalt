<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ShopModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'delete_flag')->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'rank')->textInput()->hiddenInput()->label(false); ?>
    
    <hr />
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '登録') : Yii::t('app', '編集'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
