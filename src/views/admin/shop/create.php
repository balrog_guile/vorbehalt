<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ShopModel */

$this->title = Yii::t('app', 'ショップ登録');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ショップ一覧'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-model-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <hr />
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
