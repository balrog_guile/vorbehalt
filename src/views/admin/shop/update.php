<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShopModel */

$this->title = Yii::t('app', 'ショップ編集');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ショップ一覧'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', '編集');
?>
<div class="shop-model-update">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <hr />
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
