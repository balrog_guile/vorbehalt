<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SettingsModel */

$this->title = Yii::t('app', '基本設定');
?>
<div class="settings-model-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr />
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
