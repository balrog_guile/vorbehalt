<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SettingsModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
        $form
            ->field($model, 'reservation_types')
            ->dropDownList(
                [
                    '1' => '自由入力',
                    '2' => '自由入力（時間単位）',
                    '3' => '時間割入力（単数）',
                    '4' => '時間割入力（複数選択可）'
                ],
                []
            )
    ?>

    <?= $form->field($model, 'interval')->textInput() ?>

    <?= $form->field($model, 'init_buffer')->textInput() ?>
    
    <?=
        $form
            ->field($model, 'auth_mail')
            ->dropDownList(
                [
                    '0' => '不要',
                    '1' => '必要',
                ],
                []
            )
    ?>
    
    
    <hr />
    
    <div class="form-group">
        <?= Html::submitButton(
                Yii::t('app', '設定保存'),
                [
                    'class' => 'btn btn-primary'
                ]
            )
        ?>
    </div>
    
    <hr />
    
    <?php ActiveForm::end(); ?>

</div>
