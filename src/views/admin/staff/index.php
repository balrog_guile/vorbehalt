<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StaffSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Staff Models');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Staff Model'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'shop_id',
            'name',
            'display_name',
            'kana',
            // 'staff_status',
            // 'create_at',
            // 'update_at',
            // 'delete_flag',
            // 'deleted_at',
            // 'rank',
            // 'shop_role',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
